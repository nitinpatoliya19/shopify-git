const { series, src, dest, watch, parallel } = require('gulp');
const del = require('del');
const webpack = require('webpack-stream');
const webpackConfig = require('./webpack.config');
const flatten = require('gulp-flatten');
const execSync = require('child_process').execSync;
const waitOn = require('wait-on');
const fs = require('fs');
const sass = require('gulp-sass');
const rename = require('gulp-rename');
sass.compiler = require('node-sass');

const gulpfile = (() => {
  
  const config = {
    dir: {
      src: 'src',
      dist: 'dist',
      ts: 'scripts',
      scss: 'styles',
      icons: 'icons'
    },
    files: [
      '/config/*.json',
      '/locales/*.json',
      '/layout/*.liquid',
      '/templates/customers/*.liquid',
      '/templates/*.liquid',
      '/snippets/*.liquid',
      '/sections/*.liquid',
      '/assets/*'
    ],
    style: 'theme.scss',
    webpack: webpackConfig,
    watch: {
      stabilityThreshold: 300,
      pollInterval: 300
    }
  }

  const clean = (cb) => {
    del.sync(`./${ config.dir.dist }`);
    cb();
  }

  const build = async (cb) => {
    try {
      await files();
      await icons();
      await webpackLoaders();
      await css();
      await waitOn({ resources: [ config.dir.dist ] });
    } catch(error) {
      console.error(error);
    }
    cb();
  }

  const files = () => {
    return new Promise(function(resolve, reject) {
      src(config.files, { base: './', root: `./${ config.dir.src }` })
      .on('error', reject)
      .pipe(rename(function(path) {
        path.dirname = path.dirname.replace(config.dir.src, '');
      }))
      .pipe(dest(`./${ config.dir.dist }`))
      .on('end', resolve);
    });
  }

  const icons = () => {
    return new Promise(function(resolve, reject) {
      src(`${ config.dir.src }/${ config.dir.icons }/*`)
      .on('error', reject)
      .pipe(rename((path) => { path.extname = '.liquid'; }))
      .pipe(dest(`${ config.dir.dist }/snippets`))
      .on('end', resolve);
    })
  }

  const webpackLoaders = () => {
    return new Promise(function(resolve, reject) {
      src(config.webpack.entry)
      .on('error', reject)
      .pipe(webpack(config.webpack))
      .pipe(dest(`${ config.dir.dist }/assets`))
      .on('end', resolve);
    });
  }

  const css = () => {
    return new Promise(function(resolve, reject) {
      src(`${ config.dir.src }/${ config.dir.scss }/${ config.style }`)
      .pipe(sass().on('error', sass.logError))
      .pipe(sass({outputStyle: 'compressed'}))
      .pipe(dest(`${ config.dir.dist }/assets`))
      .on('end', resolve);
    })
  }

  const watchSrc = () => {
    watch([
      './src/**/*',
      `!./src/${ config.dir.icons }/*`,
      `!./src/${ config.dir.ts }/**/*.ts`, 
      `!./src/${ config.dir.scss }/**/*.scss`
    ]).on('change', function (path) {
      src(path).pipe(flatten()).pipe(dest(getDir(path)));
    }).on('add', function (path) {
      src(path).pipe(flatten()).pipe(dest(getDir(path)));
    }).on('unlink', function (path) {
      del.sync(updateDir(path));
    });
    watch([
      `./src/${ config.dir.icons }/*`,
      `!./src/${ config.dir.ts }/**/*.ts`, 
      `!./src/${ config.dir.scss }/**/*.scss`
    ]).on('change', function (path) {
      src(path).pipe(rename((path) => {
        path.extname = '.liquid';
      })).pipe(flatten()).pipe(dest(getDir(path)));
    }).on('add', function (path) {
      src(path).pipe(rename((path) => {
        path.extname = '.liquid';
      })).pipe(flatten()).pipe(dest(getDir(path)));
    }).on('unlink', function (path) {
      del.sync(updateDir(path));
    });
    watch([`./src/${ config.dir.ts }/**/*.ts`], awaitWriteFinish()).on('all', async function() {
      await webpackLoaders();
    });
    watch([`./src/${ config.dir.scss }/**/*.scss`], awaitWriteFinish()).on('all', async function() {
      await css();
    });
  }
  
  const getDir = (path) => {
    return updateDir((path.split('\\').reduce((acc, dir, i, src) => {
      if (i !== src.length - 1) acc = acc + dir + '/';
      return acc;
    }, '')));
  }
  
  const updateDir = (path) => {
    if (path.includes('icons')) path = path.replace('icons', 'snippets');
    return path.replace(config.dir.src, config.dir.dist);
  }
  
  const awaitWriteFinish = () => {
    return { 
      awaitWriteFinish: {
        stabilityThreshold: config.watch.stabilityThreshold,
        pollInterval: config.watch.pollInterval
      }
    }
  }

  return {
    clean,
    build,
    watchSrc
  }

})();

exports.clean = series(gulpfile.clean);
exports.build = series(gulpfile.clean, gulpfile.build);
exports.watch = series(gulpfile.watchSrc);
exports.default = series(gulpfile.clean, gulpfile.build, gulpfile.watchSrc);