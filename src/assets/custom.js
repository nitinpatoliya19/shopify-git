$(document).ready(function() {    
	$(window).scroll(function() {
      if($(window).width() > 1024){	
        $(".header").headroom({
          "offset": 205,
          "tolerance": 5,
          "classes": {
            "initial": "animated",
            "pinned": "slideDown",
            "unpinned": "slideUp"
          }
        });
      }
    });      
  
    /*
    var grid = document.querySelector('.grid');
    var msnry = new Masonry( grid, {
      itemSelector: '.grid-item',
      columnWidth: '.grid-sizer',
      gutterWidth: '.grid-gutter'
    });

    imagesLoaded( grid ).on( 'progress', function() {
      // layout Masonry after each image loads
      msnry.layout();
    });
    
  
    $('.grid').packery({
      // options
      itemSelector: '.grid-item',
      percentPosition: true,
      gutter: 10
    });
    */
    var $grid = $('ul.gridproducts').packery({
      // options
      itemSelector: '.block'
    });
    // layout Packery after each image loads
    $grid.imagesLoaded().progress( function() {
      $grid.packery();
    });
  
   /* $( ".cmt-title" ).click(function() {
	  $( ".cmt-info" ).slideToggle( "slow", function() {
		if ( $( ".cmt-title" ).hasClass( "cmt-hide" ) ) {
		  $( ".cmt-title" ).addClass( "cmt-show" );
		  $( ".cmt-title" ).removeClass( "cmt-hide" );
		} else {
		  $( ".cmt-title" ).addClass( "cmt-hide" );
		  $( ".cmt-title" ).removeClass( "cmt-show" );
		}
	  });
	});
  
    $( ".product_section .description h2" ).click(function() {
	  $( ".product_section .description span" ).slideToggle( "slow", function() {
		if ( $( ".product_section .description h2" ).hasClass( "cmt-hide" ) ) {
		  $( ".product_section .description h2" ).addClass( "cmt-show" );
		  $( ".product_section .description h2" ).removeClass( "cmt-hide" );
		} else {
		  $( ".product_section .description h2" ).addClass( "cmt-hide" );
		  $( ".product_section .description h2" ).removeClass( "cmt-show" );
		}
	  });
	});*/
      

    $(window).on("load", function() {
      bannerheight();
    });

    $(window).on("resize", function() {
      bannerheight();
    });

    function bannerheight() {
      $hie = $(window).height();
      $('.img-bg').css('min-height', $hie - 300 + 'px');
    }
  
  
  	if($("body").hasClass("product-a")) {
      tmpVal = tmpComma = '';
      $("input[name='properties[size]']").on('change',function(){
        formatLetter();
      });
      
      $("input[name^='properties[letter']").keyup(function(){
        formatLetter();
      });

      $("input[name='properties[dots]']").on('change',function(){
        formatLetter();
      });
      
      $(".swatch_options input[name='option-0']").on('change',function(){
        formatLetter();
      });
      
      function formatLetter(){      
        
        tmpFont = '';
        $("input[name='properties[size]']:checked").each(function() {
          tmpFont = $(this).val();
          if($(this).val() == 'SMALL')
            $('.product-img-custom').removeClass('LargeFont').addClass('SmallFont');
          else
            $('.product-img-custom').removeClass('SmallFont').addClass('LargeFont');          
        });
        
        tmpDots = '';
        $("input[name='properties[dots]']:checked").each(function() {
          tmpDots = $(this).val();
        });      
        
        tmpColor = $(".swatch_options input[name='option-0']:checked").val().toLowerCase();
        if(tmpColor == 'peach')
          tmpColor = '#fab171';        
        //console.log(tmpColor);

        tmpVal = tmpComma = '';
        $("input[name^='properties[letter']").each(function(){
          if($(this).val() != ''){
            tmpVal = tmpVal + tmpComma + $(this).val();
            if(tmpDots == 'WITH') 
              tmpComma = "."; //tmpComma = "&bull;";
            else
              tmpComma = "";
            //console.log(tmpDisplay);
          }
        });
                
        $('.product-img-custom').html(tmpVal).css({'color':tmpColor});
      }
    }
  
  	$("#personalisebtn").click(function() {
      $(this).hide();
      $(".personalise").show();      
    });
});

/*BCCUSTOM*/
$(document).ready( function() {
    $.getScript('//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js', function() {
      $("#date").datepicker( {
        autoclose: true,
        todayHighlight: true,
        showOtherMonths: true,
        selectOtherMonths: true,
        changeMonth: true,
        changeYear: true,
        minDate: +1
      } );
      $('#date').datepicker('setDate');
    });
  });



$(document).ready(function(){
$('.navicon').click( function(){
    if ( $(this).hasClass('close_btn') ) {
        $(this).removeClass('close_btn');
    } else {
        $('.navicon span').removeClass('close_btn');
        $(this).addClass('close_btn');    
    }
});
});





