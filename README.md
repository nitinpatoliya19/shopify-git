# Accelerator Theme

The Accelerator Theme serves as the basis for new Shopify Themes built by Digital Boutique. It uses [Gulp Tasks](https://gulpjs.com/docs/en/getting-started/creating-tasks/) to compile the `src` directory into the `dist` and [Shopify Theme Kit](https://shopify.github.io/themekit/) to upload those files into your Shopify Enviroment. Under the hood it uses TypeScript, SASS and Liquid Templates.

## Required Apps & Packages

In order to develop with the Accelerator Theme you'll need [Shopify Theme Kit](https://shopify.github.io/themekit/) and [Gulp](https://gulpjs.com/docs/en/getting-started/quick-start/) installed globally on your system. You'll also need to ensure [Accentuate Custom Fields](https://apps.shopify.com/accentuate) is installed on your [Shopify Enviroment](https://help.shopify.com/en/manual/shopify-admin).

## Getting Started

### Setting up an existing Shopify environment
  
- `git clone` the relevant repository

- Duplicate the live theme inside your [Shopify Enviroment](https://help.shopify.com/en/manual/shopify-admin)

- Create a [config.yml](https://shopify.github.io/themekit/configuration/#config-file)

- Run `gulp build`

- Run `theme deploy`

- Run `gulp watch`, in a separate terminal run `theme watch`.

### Setting up a new Shopify environment

- Fork the Accelerator Theme repository.

- Create a development theme inside a new [Shopify Enviroment](https://help.shopify.com/en/manual/shopify-admin)

- Modify your [config.yml](https://shopify.github.io/themekit/configuration/#config-file)

- Run `gulp build`

- Run `theme deploy`

- Run `gulp watch`, in a separate terminal run `theme watch`.
## Deployments

Never deploy directly to a live theme. Always create a duplicate of the live theme, then push the latest `master` branch to the duplicate theme. The preview link can then be supplied for configuration and approval by the client. Upon approval the theme may be published. Always set your `settings_data.json` and `locales/*` file to ignore when deploying, otherwise we risk over-writing clients configuration. 

### Deploying to a Shopify Enviroment

- Duplicate the live theme inside your [Shopify Enviroment](https://help.shopify.com/en/manual/shopify-admin)

- Modify your [config.yml](https://shopify.github.io/themekit/configuration/#config-file)

- Run `gulp build`

- Run `theme deploy`

## Commands
The following gulp commands can be used to perform various build tasks.

|Command|Description|
|--|--|
`gulp`|Runs `gulp clean`, `gulp build` and `gulp watch` in series.|
|`gulp clean`| Removes the existing `dist` directory.|
|`gulp build`| Runs the assets pipelines and builds the `dist` directory.|
|`gulp watch`| Watches for changes to the `src` directory, and then pushes those changes to the `dist` directory through the asset pipelines|

For information on available Theme Kit commands [click here](https://shopify.github.io/themekit/commands/).

## Required pages for new Shopify Enviroments

The Accelerator Theme relies on additional pages to handle reloading the cart and mini basket snippets using AJAX. You'll need to set these pages up when creating a new theme. The Accelerator preview theme contains examples of how to configure the [mini basket](https://db-accelerator.myshopify.com/admin/pages/73838297279) and [cart](https://db-accelerator.myshopify.com/admin/pages/73482502335) pages.


## Accentuate Custom Fields

The Accelerator Theme makes use of Accentuate to assist in additional configuration when global settings won't suffice. You'll need to ensure it's installed when you want to make use of these additional features, or to add new ones.

You can view how the existing features are configured by [clicking here](https://db-accelerator.myshopify.com/admin/apps/accentuate).

# Liquid

The Liquid templates are available in the `/src/templates/*`, `/src/icons/*`, `/src/layout/*`, `/src/snippets/*` and `/src/sections/*` folders in the Accelerator Theme.

Templates are used to render sections on the page.

Sections are used to define the grid layout of snippets. They contain HTML elements that include grid classes, such as `wrapper`, `container`, `row` and `col-sm-*` as well as utility classes such as `background--*` or `margin-md--*`.
 
Snippets contain no grid classes on their HTML elements, styling is self-contained inside a corresponding file found in the `src/styles/snippets/*.scss` folder. You may [pass liquid variables](https://shopify.dev/docs/themes/liquid/reference/tags/theme-tags#render) to snippets.
  
## Common Snippets 

The following is a list of some of the most common snippets rendered throughout the theme, however, this list is not comprehensive. It is recommend any new developer takes the time to browse the `src/snippet/*.liquid` files to understand what snippets are available for rendering on the page. 

Whenever possible an existing snippet should be used instead of creating a new one. This helps enable us to create standard snippets across the theme that can be modified globally. 

Some snippets render other snippets, you can think of this as a parent child relationship and pass your variables from the parent snippet into the child snippet. 

### src/snippets/img.liquid

The img.liquid should always be used when rendering images. 

#### Example

  {%-
    render 'img',
    lazy: true,
    img_size: '640x',
    img: product.image
  -%}

#### Properties

  
| Key | Type |Description|
|--|--|--|
| lazy | boolean | Enables lazy loading|
| img | [image](https://shopify.dev/docs/themes/liquid/reference/objects/image) | Pass an image object that gets rendered for both mobile and desktop.|
| img_desktop |[image](https://shopify.dev/docs/themes/liquid/reference/objects/image) |Pass an image object that gets rendered on desktop.|
|img_mobile|[image](https://shopify.dev/docs/themes/liquid/reference/objects/image) |Pass an image object that gets rendered on mobile.|
|img_size|string|Pass the image render size for the image object associated with the `img` variable. e.g. `300x`|
|img_desktop_size|string|Pass the image render size for the image object associated with the `img_desktop` variable. e.g. `300x`|
|img_mobile_size|string|Pass the image render size for the image object associated with the `img_mobile` variable. e.g. `300x`|

### src/snippets/hero.liquid

The hero.liquid is used to render heroes onto the page. It can be rendered with or without an image object.

#### Example of a Hero with an image.

    {%-
	    render  'hero',
	    img_desktop: section.settings.img_desktop,
	    img_mobile: section.settings.img_mobile,
	    cta_heading: section.settings.cta_heading,    
	    cta_copy: section.settings.cta_copy,    
	    cta_btn_text_1: section.settings.cta_btn_text_1,    
	    cta_btn_url_1: section.settings.cta_btn_url_1,    
	    class_list: 'hero--full-bleed hero--gradient',
	    heading_size: 'h2'
    -%}

#### Example of a Hero without an image.

  {%- 
		render 'hero',
		class_list: 'hero--no-image',
		cta_heading: 'account.heading',
		is_translated: true,
		heading_size: 'h1' 
	-%}

#### Properties

| Key | Type |Description|
|--|--|--|
|class_list|string|Additional classes to be added to the parent element. Can be used to pass modifiers, for example `hero---no-image`.|
|no_lazy|boolean|Removes lazy loading on the hero image.|
|is_translated|boolean|Enabled using a translation instead of a string when rendering the CTA header.|
|img_mobile|[image](https://shopify.dev/docs/themes/liquid/reference/objects/image)|The image object to be rendered on mobile.|
|img_desktop|[image](https://shopify.dev/docs/themes/liquid/reference/objects/image)|The image object to be rendered on desktop.|
|img_desktop_size|string|Pass the image render size for the image object associated with the `img_desktop` variable. e.g. `300x`|
|img_mobile_size|string|Pass the image render size for the image object associated with the `img_mobile` variable. e.g. `300x`|
|cta_heading|string or [translation](https://shopify.dev/tutorials/develop-theme-localization-use-translation-keys)|The heading for the CTA snippet of the Hero.|
|cta_copy|string|The copy for the CTA snippet of the Hero.|
|heading_size|`'h1'` or `'h2'`, defaults to `'h3'` | The type of heading HTML element to be rendered in the CTA snippet.|

### src/snippets/cta.liquid

The CTA snippet is used to render a standard Call To Action on the page. It contains a heading, copy and two buttons. It is used within the `src/snippets/hero.liquid` file, but can be rendered elsewhere if required.

#### Example

    {%-
	    render  'cta',
	    heading: cta_heading,
	    copy: cta_copy,    
	    btn_text_1: cta_btn_text_1,	    
	    btn_url_1: cta_btn_url_1,    
	    heading_size: heading_size	    
    -%}

#### Properties
| Key | Type |Description|
|--|--|--|
|heading|string|The heading used in the CTA.|
|copy|string|The copy used in the CTA.|
|btn_text_1|string|The text used for the first CTA button.|
|btn_url_1|string|The URL used for the first CTA button.|
|btn_text_2|string|The text used for the second CTA button.|
|btn_url_2|string|The URL used for the second CTA button.|
|heading_size|`'h1'` or `'h2'`, defaults to `'h3'` | The type of heading HTML element to be rendered in the CTA snippet.|

### src/snippets/product-tile.liquid

The product tile is used to render a product tile. Used in the grid or recently viewed for example.

#### Example

	{%- 
		render  'product-tile',
		product: product,
		img_size: '350x',
		no_price: true
	-%}

#### Properties

| Key | Type |Description|
|--|--|--|
|product|[product](https://shopify.dev/docs/themes/liquid/reference/objects/product) or [line_item](https://shopify.dev/docs/themes/liquid/reference/objects/line_item)|The  line item or product object.|
|img_size|string|The render size of the product image.|
|no_price|boolean|Don't render the product tile price.|

### src/snippets/product-price.liquid

The product price snippet should be used whenever you are rendering pricing.

#### Example

  {%-
    render  'product-price'
    product: line_item,
    price: line_item.final_price,
    compare_at_price: line_item.original_price    
  -%}
    
#### Properties

| Key | Type |Description|
|--|--|--|
|product|[product](https://shopify.dev/docs/themes/liquid/reference/objects/product) or [line_item](https://shopify.dev/docs/themes/liquid/reference/objects/line_item)|The  line item or product object.|
|price|string|The price of the product or line item.|
|compare_at_price|string|The text used for the first CTA button.|
|data|string|Additional data attributes to be attached to the parent HTML element.|

### src/snippets/featured-collection.liquid

The featured collection is used to render a collection in a slider, it is commonly used for suggested products on the product template aswell as upsells on the homepage and cart page.

#### Example

    {%-
      render  'featured-collection',
      collection: section.settings.collection,
      heading: section.settings.heading,
      description: section.settings.description
    -%}

#### Properties

| Key | Type |Description|
|--|--|--|
|collection|[collection handle](https://shopify.dev/docs/themes/liquid/reference/objects/collection#collection-handle)|The collection handle for the collection to be rendered.|
|heading|string|The featured collection heading.|
|description|string|The featured collection description.|

## TypeScript

Typescript is transpiled and bundled by WebPack. You can configure WebPack options by modifying the `webpack.config.json`. WebPack is called inside the `gulpfile.js`.  

Only the `/src/scripts/theme.ts` is transpiled, all other TypeScript files are imported from the `/src/scripts/theme.ts`.

## Syntax and Patterns

All modules and helpers use the class syntax. The default export is a single instance of the class.

    class Example {
	    /* Some code */
    }
    
    export default new  Example();

When you want to initialise functionality on a page load you should create an `init()` method. You should then call this method inside the `app.ts`  `load()` method.

    class Example {
      public init() {
        console.log('foo');
      }
    }
    
    export default new  Example();

Then inside the `app.ts`

    export default class App {
      public init() {
        document.addEventListener('DOMContentLoaded', this.load);
      }
      private load() {
        example.init();
      }
    }

## /scripts/modules/*.ts

Generally, modules are related to a specific snippet using data attributes. For example, in the case of the `addToCart.ts` module all the related data attributes can be found in the `add-to-cart.liquid` snippet. This helps keep the modules specified to avoid issues with dependencies on other snippets.

## /scripts/helpers/*.ts

This is not a comprehensive list of all helpers, but features the most commonly implemented. I recommend taking time to browse the `/src/scripts/helpers/*.ts` files.

### src/scripts/helpers/cart.ts

The `cart.ts` helper can be used to modify the users cart. It is a wrapper around the existing [Shopify Cart AJAX API](https://shopify.dev/docs/themes/ajax-api/reference/cart) and allows requests to be returned as promises. 

#### Methods

|Method|Description|
|--|--|
`cart.add()`| Adds an item to the cart.|
`cart.get()`|Gets the current cart|
`cart.update()`|Updates the existing cart|,
`cart.change()`|Changes the existing cart|,
`cart.clear()`|Clears the cart.

For more information on the above methods and which options to pass to each method [click here](https://shopify.dev/docs/themes/ajax-api/reference/cart).

#### Example

    try {
      await  cart.add({
        items: [
          {    
          quantity: 1,
            id: 123456789
          }    
        ]
      });
    }
    catch(error) {
      log.error(error);
    }

### src/scripts/helpers/template.ts
The `template.ts` helper allows you to easily load HTML from another page using AJAX and either append or insert it into your current page. 

The `template.ts` methods return a `promise`.
#### Methods
|Method|Description|
|--|--|
`template.getAndUpdateHTML()`| Gets HTML from another URL and inserts it into the page.|
`template.getAndAppendHTML()`|Gets HTML from another URL and appends it to the page.|

#### Example

Inserting HTML

    await template.getAndUpdateHTML({
	    node:  document.querySelector('[data-line-item-table]'),
	    content:  '[data-line-item-table-content]',
	    url:  '/pages/ajax-cart'
    });

Appending HTML

    await template.getAndAppendHTML({
	    node:  this.config.htmlElement,
	    content:  '[data-collection-grid-page]',
	    url:  window.location.pathname + `?page=2`,
	    view:  'product-tiles'
    });

### src/scripts/helpers/storefrontSearch.ts
The `storefrontSearch.ts` module allows you to generate URLs for the [storefront search](https://help.shopify.com/en/manual/online-store/os/storefront-search).

#### Methods
|Method|Description|
|--|--|
`storefrontSearch.query()`| Returns a URL for the storefront search based on the provided options. Often used in conjunction with the `template.ts` helper.|

#### Example

    const  query = storefrontSearch.query({
	    type:  'product',
	    operator:  'OR',
	    view:  'json',
	    tags: [
		    {
			    key:  'group',
			    value:  id
		    }
	    ]
    });

### src/scripts/helpers/log.ts
The `log.ts` is used to output to the console, for example logging errors in a catch statement. It exists as a wrapper around the generic `console` methods in order to easily integrate any error monitoring systems if required.

#### Methods
|Method|Description|
|--|--|
`log.error()`| Logs a `console.error()`.|
#### Example

    try {
	    response = JSON.parse(response.data.body.innerText);
    }
    catch(error) {
	    log.error(error);
    }


### src/scripts/helpers/currency.ts

When handling prices in Shopify they are returned by default without formatting,  such as `1000`, the currency module allows you to convert these prices into formatted price strings, such as `$10.00`.
#### Methods
|Method|Description|
|--|--|
`currency.format()`| Formats a Shopify Price to a price string.|
`currency.parseFromStringToPrice()`| Formats a price string to a Shopify Price.|

#### Example

	currency.format(1000); // returns $10.00
	currency.parseFromStringToPrice('$10.00'); // returns 1000

### src/scripts/helpers/slider.ts

The `slider.ts` helper acts as a wrapper around the [TinySlider](https://github.com/ganlanyuan/tiny-slider) plug-in. It should be used whenever you want to create new sliders on the page.

For all available options [click here](https://github.com/ganlanyuan/tiny-slider#options).
#### Methods
|Method|Description|
|--|--|
`slider.init()`| Initialises a slider on the node passed in the first parameter.|

#### Example

	slider.init(document.querySelector('.my-slider'), { items:  4.5 });

### src/scripts/helpers/slideout.ts

The `slideout.ts` helpers acts as a wrapper around the [SlideoutJS](https://slideout.js.org) plug-in. It should be used whenever you want to create new slideouts on the page.

For all available options [click here](https://github.com/mango/slideout#usage).
#### Methods
|Method|Description|
|--|--|
`slideout.init()`| Initialises a slideout on the node passed in the first parameter.|

#### Example
    slideout.init(document.querySelector('.my-slider'), {
	    side:  'right'    
    });
### src/scripts/helpers/productGroup.ts

The `productGroup.ts` helper allows you to return an array of JSON data for a specific product group. It uses an alternate search template to return the JSON data and the `storefrontSearch.ts` module to generate the query.

Groups are defined by tagging a product with a key value pair where the key is Group. This can be done like so `Group: Example`.  All products with a group tag present with a matching value will be grouped.

The JSON returned can be modified by editing the `search-json-product.liquid` snippet.
#### Methods
|Method|Description|
|--|--|
`productGroup.getJSON()`| Returns an array of products based on the current products `Group:` tag.|

#### Example

    let products = null;
    
    try {
      products = await productGroup.getJSON();
    } catch(error) {
      log.error(error);
    }

